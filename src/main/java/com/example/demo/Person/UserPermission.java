package com.example.demo.Person;

public enum UserPermission {

	EMPLOYEE_READ("employee:read"),
	EMPLOYEE_WRITE("employee:write"),
	JOB_READ("job:read"),
	JOB_WRITE("job:write");
	private final String Permission;

	private UserPermission(String permission) {
		Permission = permission;
	}

	public String getPermission() {
		return Permission;
	}
	
	
}	
