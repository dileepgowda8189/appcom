package com.example.demo.Person;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
@Document(collection = "user")
public class Person {
	@Id
	String id;
	String pName;
	String email;
	String password;
	Person(){
		}
	
	public Person(String id, String pName, String email, String password ) {
		this.id = id;
		this.pName = pName;
		this.email = email;
		this.password = password;
		 
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getpName() {
		return pName;
	}
	public void setpName(String pName) {
		this.pName = pName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Person [id=" + id + ", pName=" + pName + ", email=" + email + ", password=" + password + "]";
	}
	 
	
}
